/*
	Simulates an obfuscated BitTorrent Handshake between two peers
	and outputs the request packet payload bytes on screen.
*/
import java.util.Random;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.lang.StringBuilder;
import java.nio.ByteBuffer;

object Hex {
	def valueOf(buf: Array[Byte]): String = buf.map("%02X" format _).mkString

	def hex2Bytes( hex: String ): Array[Byte] = {
		(for { i <- 0 to hex.length-1 by 2 if i > 0 || !hex.startsWith( "0x" )}
		yield hex.substring( i, i+2 )).map( Integer.parseInt( _, 16 ).toByte ).toArray
	}

	def bytes2Hex( bytes: Array[Byte] ): String = {
		def cvtByte( b: Byte ): String = {
			val v: Byte = (b & 0xff).toByte;
			(if (v < 0x10 ) "0" else "") + v.toString;
		}
		"0x" + bytes.map( cvtByte( _ )).mkString.toUpperCase
	}
}

class PeerID (val rnd: Random) {
	val data = generateMagic;
	var tries = 0;
	
	def generateMagic(): Array[Byte] = {
		var notFound = true;
		val peerid = new Array[Byte](20);
		while (notFound) {
			tries += 1;
			rnd.nextBytes(peerid);
			notFound = !isMagic(peerid);
		}
		return peerid;
	}

	def isMagic(peerid: Array[Byte]): Boolean = {
		val md = MessageDigest.getInstance("SHA-1");
		md.update(peerid);
		val digest = md.digest();
		val magic: Int = ((digest(digest.length-2) << 8) | digest(digest.length-1));
		return ((magic & 0xFFFF) != 0);
	}
	
	override def toString(): String = Hex.valueOf(data);
}

class InfoHash  (val rnd: Random) {
	val data = new Array[Byte](20);
	rnd.nextBytes(data);
	override def toString(): String = Hex.valueOf(data);
}

object Padding {
	def craft(rnd: Random): Padding = {
		val typ: Byte = 0xa;
		val len: Int = rnd.nextInt(1400-20-68-5);
		val pad = new Array[Byte](len);
		rnd.nextBytes(pad);
		val buf = ByteBuffer.allocate(5 + len);
		buf.putInt(len); buf.put(typ); buf.put(pad);
		new Padding(buf.array);
	}
	
	def parse(data: Array[Byte]): Padding = new Padding(data);
}

class Padding (val data: Array[Byte]) {
	val typ = data(0);
	val len: Int = ByteBuffer.wrap(data,1,4).getInt();
	val pad = data.slice(5,data.length);
	
	override def toString(): String = Hex.valueOf(data);
}

object Handshake {
	def craft(rnd: Random, local: PeerID, remote: PeerID, ih: InfoHash, rsvd: Array[Byte]): Handshake = {
		var salt = new Array[Byte](8);
		rnd.nextBytes(salt);
		val md = MessageDigest.getInstance("SHA-1");
		val rsi = Array.concat(remote.data, salt);
		md.update(rsi);
		val digest = md.digest();
		md.reset();
		
		val hs = new Array[Byte](68);
		var p = 0; // hs pointer
		// obfuscate 12 first byte
		Array.copy(digest,0,hs,p,12); p += 12;
		// salt
		Array.copy(salt,0,hs,p,salt.length); p += salt.length;
		 // reserved
		val rsvdmask = digest.slice(12,20);
		for (i <- 0 until rsvd.length) hs(p+i) = (rsvd(i) ^ rsvdmask(i)).toByte;
		p += rsvd.length;
		// info hash
		for (i <- 0 until ih.data.length) hs(p+i) = (ih.data(i) ^ digest(i)).toByte;
		p += ih.data.length;
		// peer id
		for (i <- 0 until local.data.length) hs(p+i) = (local.data(i) ^ digest(i)).toByte;
		p += local.data.length;
		// padding
		val pad = Padding.craft(rnd);
		for (i <- 0 until 5) pad.data(i) = (pad.data(i) ^ digest(i)).toByte;
		new Handshake(Array.concat(hs,pad.data))
	}
	
	def parse(data: Array[Byte]): Handshake = new Handshake(data);
}

class Handshake (val data: Array[Byte]) {
	val signature = data.slice(0,12);
	val salt = data.slice(12,20);
	val reserved = data.slice(20,28);
	val infohash = data.slice(28,48);
	val peerid = data.slice(48,68);
	val pad = Padding.parse(data.slice(68,data.length));
		
	def isValid(ih: InfoHash, pid: PeerID): Boolean = {
		val md = MessageDigest.getInstance("SHA-1");
		val rsi: Array[Byte] = Array.concat(pid.data, salt);
		md.update(rsi);
		val digest = md.digest();
		md.reset();
		// sig
		if (digest.slice(0,12).deep != signature.deep) {
			println("dig: " + Hex.valueOf(digest.slice(0,12)));
			println("sig: " + Hex.valueOf(signature));
			return false;
		}
		// ih
		val ih_decoded = new Array[Byte](20);
		for (i <- 0 until infohash.length) ih_decoded(i) = (infohash(i) ^ digest(i)).toByte;
		if (ih_decoded.deep != ih.data.deep) return false
		return true;
	}
	
	override def toString(): String = Hex.valueOf(data);
	
	def toPgm(wid: Int): String = {
		val maxwid = math.min(wid,data.length)
		val delta = if (wid > data.length) wid - data.length else 0;
		var s = ""
		for (i <- 0 until maxwid) s += "%4d".format(0xff & data(i).asInstanceOf[Int])
		if (delta > 0) {
			for (i <- 0 until delta) s += "%4d".format(0);
		}
		return s;
	}
	
	def toCsv(wid: Int): String = {
		val maxwid = math.min(wid,data.length)
		val delta = if (wid > data.length) wid - data.length else 0;
		var s = ""
		for (i <- 0 until maxwid) s += "%3d,".format(0xff & data(i).asInstanceOf[Int])
		if (delta > 0) {
			for (i <- 0 until delta) s += "%3d,".format(0);
		}
		return s.dropRight(1);
	}
}

class Peer (val rnd: Random, val ih: InfoHash) {
	val pid = new PeerID(rnd);
	val rsvd = new Array[Byte](8);
	
	def request(remote: Peer): Handshake = {
		Handshake.craft(rnd, pid, remote.pid, ih, rsvd);
	}
	
	def respond(remote: Peer, hs: Handshake): Handshake = {
		if (hs.isValid(ih,pid)) Handshake.craft(rnd, pid, remote.pid, ih, rsvd)
		else null;
	}
	
	override def toString(): String = pid.toString;
}

object Simulator extends Application {
	val rnd = new Random(System.currentTimeMillis());
	val ih = new InfoHash(rnd);
	val lpeer = new Peer(rnd, ih);
	
	def main(args: String) {
		val ar = args.split(" ");
		if (ar.length<3) {
			println("Usage: handshake <rows> <cols> <format>");
			println("\trows: integer for number of rows.");
			println("\tcols: integer for number of columns.");
			println("\tformat: string for output format. 'pgm' or 'csv'.");
			exit();
		}
		val len = ar(0).toInt;
		val wid = ar(1).toInt;
		val out = ar(2).toLowerCase;
		
		if (out.equals("pgm")) {
			println("P2")
			println(wid + " " + len);
			println("255");
		}
		
		for (i <- 0 until len) {
			val rpeer = new Peer(rnd, ih);
			val request = lpeer.request(rpeer);
			val response = rpeer.respond(lpeer,request);
			if (out.equals("pgm")) println(request.toPgm(wid))
			if (out.equals("csv")) println(request.toCsv(wid))
		}
	}
}

var params = ""
for (a <- args) params += a + " ";
Simulator.main(params)
// Simulator.main(args) // <- does NOT work?
