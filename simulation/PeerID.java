import java.util.Random;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.lang.StringBuilder;

/*
	tests creation of magic peer ID and keeps track of the trials.
	Each time a magic peer ID has been created, the number of tries is written
	on stdout. A summary of runs, totaltries, avg tries, duration is printed
	on stdout after all runs completed.
*/
public class PeerID {
	private byte[] data;
	private int tries = 0;
	
	public PeerID(byte[] data, int tries) {
		this.data = data;
		this.tries = tries;
	}
	
	public static PeerID generateMagic() throws java.security.NoSuchAlgorithmException {
		boolean notFound = true;
		Random random = new Random(System.currentTimeMillis());
		byte[] peerid = new byte[20];
		
		int tries = 0;
		while (notFound) {
			tries++;
			random.nextBytes(peerid);
			notFound = isMagic(peerid);
		}
		//return tries;
		return new PeerID(peerid,tries);
	}
	
	public static boolean isMagic(byte[] peerid) 
	throws java.security.NoSuchAlgorithmException {
		if (peerid==null) return false;
		MessageDigest md = MessageDigest.getInstance("SHA-1");
		md.update(peerid);
		byte[] digest = md.digest();
		int magic = (digest[digest.length-2] << Byte.SIZE) | digest[digest.length-1];
		return (magic & 0xFFFF) != 0;
	}
	
	public String toCsvString() {
		StringBuilder sb = new StringBuilder();
		for (byte b: this.data) sb.append(String.format("%4d",(int)(b&0xFF))).append(",");
		String retval = sb.toString();
		return retval.substring(0,retval.length()-1);
	}
	
	public static void main (String[] args) throws java.security.NoSuchAlgorithmException {
		if (args.length < 1) {
			System.out.println("usage: java PeerID <number of tries>");
			return;
		}
		int runs = Integer.parseInt(args[0]);
		int i = runs;
		int totaltries = 0;
		System.out.printf("%6s, %4s, ", "tries", "ms");
		for (int b=0; b<19; b++) System.out.printf("%4s,", b);
		System.out.print("  20\n");
		long duration = System.currentTimeMillis();
		long trieduration;
		while (i-->0) {
			trieduration = System.currentTimeMillis();
			//int tries = PeerID.generateMagic();
			PeerID pid = PeerID.generateMagic();
			trieduration = System.currentTimeMillis() - trieduration;
			totaltries += pid.tries;
			System.out.printf("%6d, %4d, %4s\n", pid.tries, trieduration, pid.toCsvString());
		}
		duration = System.currentTimeMillis() - duration;
		/*System.out.printf("\n%d, %d, %f, %d\n",
			runs, totaltries, totaltries/(float)runs, duration);*/
	}
}
