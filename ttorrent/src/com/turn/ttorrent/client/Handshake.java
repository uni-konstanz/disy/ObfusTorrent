/** Copyright (C) 2011 Turn, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.turn.ttorrent.client;

import java.io.UnsupportedEncodingException;
import java.nio.ByteBuffer;
import java.security.NoSuchAlgorithmException;
import java.text.ParseException;
import java.util.Arrays;

import com.turn.ttorrent.common.Peer;
import com.turn.ttorrent.common.Torrent;

public class Handshake {

	public static final String BITTORRENT_PROTOCOL_IDENTIFIER = "BitTorrent protocol";
	public static final int BASE_HANDSHAKE_LENGTH = 49;

	ByteBuffer data;
	ByteBuffer infoHash;
	ByteBuffer peerId;
	
	//< obfuscated handshake extension
	private boolean isObfuscated;
	

	private Handshake(ByteBuffer data, ByteBuffer infoHash,	ByteBuffer peerId, boolean isObfuscated) {
		this.data = data;
		this.infoHash = infoHash;
		this.peerId = peerId;
		this.isObfuscated = isObfuscated;
	}

	public byte[] getSignature() {
		return Arrays.copyOfRange(data.array(), 0, 20);
	}
	
	public boolean isObfuscated() {
		return isObfuscated;
	}
	//> obfuscated handshake extension
	
	public byte[] getBytes() {
		return this.data.array();
	}

	public byte[] getInfoHash() {
		return this.infoHash.array();
	}

	public byte[] getPeerId() {
		return this.peerId.array();
	}

	//< obfuscated handshake extension
	public static Handshake parse(ByteBuffer buffer)
		throws ParseException, UnsupportedEncodingException {
		
		if (buffer.remaining() < BASE_HANDSHAKE_LENGTH + 20) {
			throw new ParseException("Incorrect handshake message length.", 0);
		}
		
		int pstrlen = new Byte(buffer.get()).intValue();
		byte[] pstr = new byte[pstrlen==0x13 ? pstrlen : 0x13];
		//byte[] pstr = new byte[0x13];
		byte[] reserved = new byte[8];
		byte[] infoHash = new byte[20];
		byte[] peerID = new byte[20];
		boolean isObfuscated = false;
				
		buffer.get(pstr);
		buffer.get(reserved);
		buffer.get(infoHash);
		buffer.get(peerID);

		// if we have an ordinary bittorrent handshake
		if (BITTORRENT_PROTOCOL_IDENTIFIER.equals(new String(pstr, Torrent.BYTE_ENCODING))) {
			isObfuscated = false;
		} /* else we might have an obfuscated handshake */ 
		else if (Peer.isMagicPeerId(peerID)) {
			isObfuscated = true;
		}
		return new Handshake(buffer, ByteBuffer.wrap(infoHash), ByteBuffer.wrap(peerID), isObfuscated);
	}

	public static Handshake craft(byte[] torrentInfoHash,
			byte[] clientPeerId, byte[] remotePeerId, boolean obfuscate) {
		if (Peer.isMagicPeerId(clientPeerId) && Peer.isMagicPeerId(remotePeerId) && obfuscate)
			return craft_obfuscated(torrentInfoHash, clientPeerId, remotePeerId, obfuscate);
		else
			return craft_standard(torrentInfoHash, clientPeerId);
	}
	
	public static Handshake craft_obfuscated(byte[] torrentInfoHash,
			byte[] clientPeerId, byte[] remotePeerId, boolean obfuscate) {
		
		// craft random padding message
		Message.PaddingMessage padding = Message.PaddingMessage.craft();
		
		/*ByteBuffer buffer = ByteBuffer.allocate(
				Handshake.BASE_HANDSHAKE_LENGTH +
				Handshake.BITTORRENT_PROTOCOL_IDENTIFIER.length());
		*/
		ByteBuffer buffer = ByteBuffer.allocate(
				Handshake.BASE_HANDSHAKE_LENGTH +
				Handshake.BITTORRENT_PROTOCOL_IDENTIFIER.length() +
				padding.length() + 5
		);
		
		byte[] reserved = new byte[8];
		ByteBuffer infoHash = ByteBuffer.wrap(torrentInfoHash);
		ByteBuffer peerId = ByteBuffer.wrap(clientPeerId);
		
		try {
			byte[] infoHashPeerId = Arrays.copyOf(torrentInfoHash, torrentInfoHash.length + remotePeerId.length);
			System.arraycopy(remotePeerId, 0, infoHashPeerId, torrentInfoHash.length, remotePeerId.length);
			byte[] sighash = Torrent.hash(infoHashPeerId);
			buffer.put(sighash, 0, Handshake.BITTORRENT_PROTOCOL_IDENTIFIER.length() + 1);
			
			byte[] peerIdInfoHash = Arrays.copyOf(remotePeerId, torrentInfoHash.length + remotePeerId.length);
			System.arraycopy(torrentInfoHash, 0, peerIdInfoHash, torrentInfoHash.length, remotePeerId.length);
			byte[] opthash = Torrent.hash(peerIdInfoHash);
			long options = ByteBuffer.wrap(reserved).getLong();
			long mask = ByteBuffer.wrap(opthash).getLong();
			options = options ^ mask;
			ByteBuffer optbuf = ByteBuffer.wrap(reserved);
			optbuf.putLong(options);
		}  catch (NoSuchAlgorithmException e) {
			obfuscate = false;
			buffer.put((byte)Handshake
					.BITTORRENT_PROTOCOL_IDENTIFIER.length());
			try {
				buffer.put(Handshake
					.BITTORRENT_PROTOCOL_IDENTIFIER.getBytes(Torrent.BYTE_ENCODING));
			} catch (UnsupportedEncodingException uee) {
				return null;
			}
		}
		
		buffer.put(reserved);
		buffer.put(infoHash);
		buffer.put(peerId);

		// add random padding
		buffer.put(padding.toBytes());
		
		return new Handshake(buffer, infoHash, peerId, obfuscate);
	}
	
	public static Handshake craft_standard(byte[] torrentInfoHash,
			byte[] clientPeerId) {
	//> obfuscated handshake extension
		try {
			ByteBuffer buffer = ByteBuffer.allocate(
				Handshake.BASE_HANDSHAKE_LENGTH +
				Handshake.BITTORRENT_PROTOCOL_IDENTIFIER.length());

			byte[] reserved = new byte[8];
			ByteBuffer infoHash = ByteBuffer.wrap(torrentInfoHash);
			ByteBuffer peerId = ByteBuffer.wrap(clientPeerId);

			buffer.put((byte)Handshake
				.BITTORRENT_PROTOCOL_IDENTIFIER.length());
			buffer.put(Handshake
				.BITTORRENT_PROTOCOL_IDENTIFIER.getBytes(Torrent.BYTE_ENCODING));
			buffer.put(reserved);
			buffer.put(infoHash);
			buffer.put(peerId);

			return new Handshake(buffer, infoHash, peerId, false);
		} catch (UnsupportedEncodingException uee) {
			return null;
		}
	}
	
}
