/** Copyright (C) 2011 Turn, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.turn.ttorrent.common;

import java.nio.ByteBuffer;
import java.security.NoSuchAlgorithmException;
import java.util.Random;

/** A basic BitTorrent peer.
 *
 * This class is meant to be a common base for the tracker and client, which
 * would presumably subclass it to extend its functionality and fields.
 *
 * @author mpetazzoni, tzink (obfuscation extension)
 */
public class Peer {

	private ByteBuffer peerId;
	private String hexPeerId;

	private String ip;
	private int port;
	private String hostId;
	
	//< obfuscated handshake extension
	private boolean supportsObfuscation;
	//> obfuscated handshake extension

	/** Instantiate a new peer for the given torrent.
	 *
	 * @param ip The peer's IP address.
	 * @param port The peer's port.
	 * @param peerId The byte-encoded peer ID.
	 */
	public Peer(String ip, int port, ByteBuffer peerId) {
		this.ip = ip;
		this.port = port;
		this.hostId = String.format("%s:%d", ip, port);

		this.setPeerId(peerId);
	}
	
	//< obfuscated handshake extension
	public static byte[] generateMagicPeerId() {
		boolean notFound = true;
		Random random = new Random(System.currentTimeMillis());
		byte[] peerId = new byte[20];
				
		while (notFound) {
			random.nextBytes(peerId);
			notFound = !Peer.isMagicPeerId(peerId);
		}
		return peerId;
	}
		
	public static boolean isMagicPeerId(byte[] peerid) {
		if (peerid==null) return false;
		byte[] digest;
		try {
			digest = Torrent.hash(peerid);
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
			return false;
		}
		int magic = (digest[digest.length-2] << Byte.SIZE) | digest[digest.length-1];
		return (magic & 0xFFFF) != 0;
	}
		
	public boolean hasMagicPeerId() {
		if (hasPeerId()) return isMagicPeerId(this.peerId.array());
		else return false;
	}
		
	public boolean supportsObfuscation() {
		return this.supportsObfuscation;
	}
		
	public void supportsObfuscation(boolean support) {
		this.supportsObfuscation = support;
	}
	//> obfuscated handshake extension

	public boolean hasPeerId() {
		return this.peerId != null;
	}

	public ByteBuffer getPeerId() {
		return this.peerId;
	}

	public void setPeerId(ByteBuffer peerId) {
		if (peerId != null) {
			this.peerId = peerId;
			this.hexPeerId = Torrent.byteArrayToHexString(peerId.array());
		} else {
			this.peerId = null;
			this.hexPeerId = null;
		}
		//< obfuscated handshake extension
			this.supportsObfuscation = this.hasMagicPeerId();
		//> obfuscated handshake extension
	}

	/** Get the hexadecimal-encoded string representation of this peer's ID.
	 */
	public String getHexPeerId() {
		return this.hexPeerId;
	}

	public String getIp() {
		return this.ip;
	}

	public int getPort() {
		return this.port;
	}

	public String getHostIdentifier() {
		return this.hostId;
	}

	/** Return a human-readable representation of this peer.
	 */
	public String toString() {
		StringBuilder s = new StringBuilder("peer://")
			.append(this.ip).append(":").append(this.port)
			.append("/");

		if (this.hasPeerId()) {
			//s.append(this.hexPeerId.substring(this.hexPeerId.length()-6));
			s.append(this.hexPeerId);
		} else {
			s.append("?");
		}

		return s.toString();
	}

	/** Tells if two peers seem to lookalike, i.e. they have the same IP and
	 * same port.
	 */
	public boolean looksLike(Peer other) {
		if (other == null) {
			return false;
		}

		return this.ip.equals(other.getIp()) && this.port == other.getPort();
	}
}
