/** Copyright (C) 2011 Thomas Zink, Uni Konstanz
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.unikn.disy;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.security.NoSuchAlgorithmException;

import com.turn.ttorrent.common.Torrent;
import com.turn.ttorrent.tracker.Tracker;

/**
 * @author zink
 */
public class MakeTorrent {

	private static String DEFAULT_ID = "anonymous";
	private static String DEFAULT_HOST = "localhost";
	
	private static void usage() {
		System.out.println("Create torrent meta information and output on " +
				"stdout");
		System.out.println("Usage: " +
				"torrent <-f filename> [-a host] [-p port] [-c creator] [-o outfile] [-h|--help]\n");
		System.out.println("\tfilename\t-\tPath of the file used to create " +
				"torrent.");
		System.out.println("\thost\t\t-\tHostname or IP of the tracker. " +
				"Default: " + DEFAULT_HOST);
		System.out.println("\tport\t\t-\tPort the tracker runs on. Default: " +
				Tracker.DEFAULT_TRACKER_PORT);
		System.out.println("\tcreator\t\t-\tName of creator or other id string." +
				" Default: " + DEFAULT_ID);
		System.out.println("\toutfile\t\t-\tOutput file name. If omitted, no" +
				" file is written.");
	}
	/**
	 * Creates torrent file information for a given file name and returns
	 * it on stdout.
	 * 
	 * @param args Filename, announce URL, creator
	 */
	public static void main(String[] args) {
		if (args.length < 2) {
			usage(); return;
		}
		
		File source = null;
		String addrstr = DEFAULT_HOST;
		int port = Tracker.DEFAULT_TRACKER_PORT;
		String createdBy = DEFAULT_ID;
		String outfile = null;
		
		int i=0;
		String arg = "";
		while (i < args.length && args[i].startsWith("-")) {
            arg = args[i++];
            if (arg.equals("--help") || arg.equals("-h")) {
                usage(); return;
            } else if (arg.equals("-a")) {
            	if (i < args.length) addrstr = args[i++];
            	else { usage(); return; }
            } else if (arg.equals("-p")) {
            	if (i < args.length) {
            		try { port = Integer.parseInt(args[i++]); }
            		catch (NumberFormatException e) { usage(); return; }
            	} else { usage(); return; }
            } else if (arg.equals("-f")) {
            	if (i < args.length) {
            		source = new File(args[i++]);
            	} else { usage(); return; }
            } else if (arg.equals("-c")) {
            	if (i < args.length) {
            		createdBy = args[i++];
            	} else { usage(); return; }
			} else if (arg.equals("-o")) {
				if (i < args.length) {
					outfile = args[i++];
				}
			}
		}
		
		try {
			if (addrstr.equals(DEFAULT_HOST)) {
				addrstr = InetAddress.getLocalHost().getHostAddress();
			} else {
				addrstr = InetAddress.getByName(addrstr).getHostAddress();
			}
		} catch (UnknownHostException e) { 
			e.printStackTrace();
		}
		
		String announce = "http://" + addrstr + ":" + port + "/announce";
		
		Torrent torrent;
		try {
			torrent = Torrent.create(source, announce, createdBy);
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
			return;
		} catch (IOException e) {
			e.printStackTrace();
			return;
		}
		
		System.err.println(torrent.toDecodedString());
		System.out.println(new String(torrent.getEncoded()));
		
		if (outfile != null) {
			try {
				FileOutputStream fos = new FileOutputStream(outfile);
				fos.write(torrent.getEncoded());
				fos.close(); 
			}
		    catch(FileNotFoundException ex) {
		    	System.out.println("FileNotFoundException : " + ex);
		    }
		    catch(IOException ioe) {
		    	System.out.println("IOException : " + ioe);
		    }
		}
	}
}
