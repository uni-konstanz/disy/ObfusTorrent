ObfusTorrent
===========

This is an implementation of several obfuscation techniques for BitTorrent clients. The BitTorrent library used is based on ttorrent 1.0.4. The obfuscation techniques are documented in two paper (see References).

The source of the BitTorrent library is in directory `ttorrent`.
The directory `simulation` contains several tools used to simulate the obfuscated behavior.

## Build the BitTorrent library distribution

See also INSTALL.

	# ant dist

## Examples

	# java -jar ttorrent-1.0.4-disy.jar 
	Runs either a client, tracker or creates a torrent metainfo file depending on arguments.

	Usage:java -jar ttorrent<version>.jar <client|tracker|create> <arguments>
		client	-	start the client
		tracker	-	start the tracker
		torrent	-	create a torrent metainfo file
		arguments	-	additional arguments passed to mainclass used.

	# java -jar ttorrent-1.0.4-disy.jar client
	usage: client <-f torrent> [-o directory] [--force | --disable]
		torrent		-	Torrent-file the client uses to share.
		directory	-	Output directory where file is put. Default: /tmp.
		--force		-	Force obfuscation at first attempt.
		--disable	-	Disable magic Peer ID generation. Overrides force.

## Use the simulation tools

### PeerID.java

Tests creation of magic peer ID and keeps track of the trials.
Each time a magic peer ID has been created, the number of tries is written
on stdout. A summary of runs, totaltries, avg tries, duration is printed
on stdout after all runs completed.

	# javac PeerID.java
	# java PeerID <number of tries>

### handshake.scala

Simulates an obfuscated BitTorrent Handshake between two peers and outputs the request packet payload bytes on screen.

	# scala handshake.scala
	Usage: handshake <rows> <cols> <format>
		rows: integer for number of rows.
		cols: integer for number of columns.
		format: string for output format. 'pgm' or 'csv'.

	# scala handshake.scala 10 15 pgm

### Obit

Simulates crafting and parsing of a random obfuscated handshake and prints all information to stdout.

	# make
	# ./obit

## References

[ZINK, Thomas; WALDVOGEL, Marcel. Bittorrent traffic obfuscation: a chase towards semantic traffic identification. In: Peer-to-Peer Computing (P2P), 2012 IEEE 12th International Conference on. IEEE, 2012. S. 126-137.](http://nbn-resolving.de/urn:nbn:de:bsz:352-202625)

[ZINK, Thomas; WALDVOGEL, Marcel. Efficient BitTorrent handshake obfuscation. In: Proceedings of the First Workshop on P2P and Dependability. ACM, 2012. S. 2.](http://nbn-resolving.de/urn:nbn:de:bsz:352-194240)

[HJELMVIK, Erik; JOHN, Wolfgang. Breaking and improving protocol obfuscation. Chalmers University of Technology, Tech. Rep, 2010, 123751. Jg.](http://www.sjalander.com/wolfgang/publications/hjelmvik_breaking.pdf)

[ttorrent](https://github.com/mpetazzoni/ttorrent)

